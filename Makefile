REGISTRY ?= registry.gitlab.com
ORG ?= merge
TAG ?= latest
IMG := $(REGISTRY)/$(ORG)/user-ui:$(TAG)

DOCKER ?= docker

.PHONY: user-ui-ctr
user-ui-ctr:
	$(DOCKER) build --no-cache -t $(IMG) .
	$(if ${PUSH}, $(DOCKER) push $(PUSH_ARGS) $(IMG))

